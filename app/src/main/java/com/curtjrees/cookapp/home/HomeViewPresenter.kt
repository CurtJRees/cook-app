package com.curtjrees.cookapp.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import coil.api.load
import com.curtjrees.cookapp.R
import com.curtjrees.cookapp.Recipe
import com.curtjrees.cookapp.utils.RecyclerViewPresenter

object RecipeItemPresenter : RecyclerViewPresenter<Recipe>() {

    override fun inflateView(parent: ViewGroup): View =
        LayoutInflater.from(parent.context).inflate(R.layout.item_recipe_large_item, parent, false)

    override fun bindData(holder: ViewHolder, data: Recipe, clickListener: ((Recipe) -> Unit)?) {
        with(holder.itemView) {
            findViewById<ImageView>(R.id.imageView).load(data.imageUrl)
            findViewById<TextView>(R.id.headingTextView).text = data.heading
            setOnClickListener { clickListener?.invoke(data) }
        }
    }
}

object HeadingItemPresenter : RecyclerViewPresenter<String>() {

    override fun inflateView(parent: ViewGroup): View =
        LayoutInflater.from(parent.context).inflate(R.layout.item_text_heading, parent, false)

    override fun bindData(holder: ViewHolder, data: String, clickListener: ((String) -> Unit)?) {
        with(holder.itemView) {
            findViewById<TextView>(R.id.headingTextView).text = data
        }
    }

}