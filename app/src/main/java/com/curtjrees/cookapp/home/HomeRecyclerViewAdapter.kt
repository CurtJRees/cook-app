package com.curtjrees.cookapp.home

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.curtjrees.cookapp.Recipe
import com.curtjrees.cookapp.utils.RecyclerViewPresenter

class HomeRecyclerViewAdapter(
    private val recipeClickListener: (Recipe) -> Unit
) : ListAdapter<HomeRecyclerViewAdapter.Item, RecyclerViewPresenter.ViewHolder>(diffCallback) {

    private val recipeItemPresenter = RecipeItemPresenter
    private val headingItemPresenter = HeadingItemPresenter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewPresenter.ViewHolder = when (viewType) {
        0 -> recipeItemPresenter.createViewHolder(parent)
        1 -> headingItemPresenter.createViewHolder(parent)
        else -> throw Exception("Unknown view type")
    }

    override fun onBindViewHolder(holder: RecyclerViewPresenter.ViewHolder, position: Int) {
        when (val item = getItem(position)) {
            is Item.RecipeItem -> RecipeItemPresenter.bindData(holder, item.recipe, recipeClickListener)
            is Item.HeadingItem -> HeadingItemPresenter.bindData(holder, item.heading)
        }
    }

    override fun getItemViewType(position: Int): Int = getItem(position).viewType


    sealed class Item(val viewType: Int) {
        data class RecipeItem(val recipe: Recipe) : Item(0)
        data class HeadingItem(val heading: String) : Item(1)
    }

    companion object {
        internal val diffCallback = object : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }
        }
    }

}