package com.curtjrees.cookapp.utils

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class RecyclerViewPresenter<T> {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    abstract fun inflateView(parent: ViewGroup): View

    abstract fun bindData(holder: ViewHolder, data: T, clickListener: ((T) -> Unit)? = null)

    fun createViewHolder(parent: ViewGroup): ViewHolder = ViewHolder(inflateView(parent))
}