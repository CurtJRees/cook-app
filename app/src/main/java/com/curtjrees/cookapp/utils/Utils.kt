//package com.curtjrees.cookapp.utils
//
//import androidx.fragment.app.Fragment
//import androidx.fragment.app.FragmentManager
//import com.curtjrees.cookapp.R
//
//object Utils {
//
//    fun switchFragment(fragmentManager: FragmentManager, fragment: Fragment, rootView: Boolean = false, shouldPopBackStack: Boolean = false) {
//        if (shouldPopBackStack) fragmentManager.popBackStack()
//
//        fragmentManager.beginTransaction().apply {
//            replace(R.id.fragContainer, fragment)
//            if (rootView) clearBackStack(fragmentManager) else addToBackStack(fragment::class.java.name)
//            setPrimaryNavigationFragment(fragment)
//            commit()
//        }
//    }
//
//    fun addFragment(fragmentManager: FragmentManager, fragment: Fragment, shouldPopBackStack: Boolean = false) {
//        if (shouldPopBackStack) fragmentManager.popBackStack()
//
//        fragmentManager.beginTransaction().apply {
//            add(R.id.fragContainer, fragment)
//            addToBackStack(fragment::class.java.name)
//            setPrimaryNavigationFragment(fragment)
//            commit()
//        }
//    }
//
//    private fun clearBackStack(fragmentManager: FragmentManager) {
//        (1..fragmentManager.backStackEntryCount).forEach { _ ->
//            try {
//                fragmentManager.popBackStack()
//            } catch (e: IllegalStateException) {
//                // There's no way of avoiding this if saveInstanceState has already been called
//                //                Timber.e(e)
//            }
//        }
//    }
//
//}