package com.curtjrees.cookapp.common

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.curtjrees.cookapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.decorView.apply {
            systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
    }


    //TODO: Look into this working with Navigation Component
    override fun onBackPressed() {
        val backPressHandled = when (val currentFrag = supportFragmentManager.primaryNavigationFragment) {
            //            is RecipeViewFragment -> (currentFrag.handleBackPress())
            else -> false
        }
        if (!backPressHandled) super.onBackPressed()
    }

}
