package com.curtjrees.cookapp.home

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.curtjrees.cookapp.R
import com.curtjrees.cookapp.Recipe
import com.curtjrees.cookapp.RecipeStep
import com.curtjrees.cookapp.utils.MarginItemDecoration
import com.curtjrees.cookapp.utils.dpToPx
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var recyclerViewAdapter: HomeRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_home, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        recyclerViewAdapter = HomeRecyclerViewAdapter { recipe ->

            val action = HomeFragmentDirections.actionHomeFragmentToRecipeViewFragment(recipe)

            val currentItem = recyclerViewAdapter.currentList.indexOfFirst { it is HomeRecyclerViewAdapter.Item.RecipeItem && it.recipe == recipe }
            val currentViewHolder = recyclerView.findViewHolderForAdapterPosition(currentItem)
            val currentView = currentViewHolder!!.itemView

            val currentImageView = currentView.findViewById<ImageView>(R.id.imageView).apply {
                transitionName = "imageViewTransition"
            }

            val currentHeadingView = currentView.findViewById<TextView>(R.id.headingTextView).apply {
                transitionName = "headingTextViewTransition"
            }

            val extras = FragmentNavigatorExtras(
                currentImageView to "imageViewTransition",
                currentHeadingView to "headingTextViewTransition"
            )


            findNavController().navigate(action, extras)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeRefreshLayout.setOnRefreshListener {
            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
            }, 500)
        }

        with(recyclerView) {
            adapter = recyclerViewAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(MarginItemDecoration(16.dpToPx()))
        }

        recyclerViewAdapter.submitList(tempList)
    }

    private val tempList = listOf(
        HomeRecyclerViewAdapter.Item.HeadingItem("New Heading"),
        HomeRecyclerViewAdapter.Item.RecipeItem(
            Recipe(
                id = 0, heading = "First Recipe", imageUrl = "https://assets.bonappetit.com/photos/5c9919258e28c80391bd50de/16:9/w_2560,c_limit/chicken-kastsu-2.jpg", steps = listOf(
                    RecipeStep(0, "First step"),
                    RecipeStep(1, "Next step")
                )
            )
        ),
        HomeRecyclerViewAdapter.Item.RecipeItem(
            Recipe(
                id = 1, heading = "Second Recipe", imageUrl = "https://assets.bonappetit.com/photos/5d56aa938bcd000008067173/16:9/w_1600%2Cc_limit/Basically-Ribs-Macro.jpg", steps = listOf(
                    RecipeStep(0, "First step"),
                    RecipeStep(1, "Next step")
                )
            )
        ),

        HomeRecyclerViewAdapter.Item.HeadingItem("Another Heading"),
        HomeRecyclerViewAdapter.Item.RecipeItem(
            Recipe(
                id = 2, heading = "Third Recipe", imageUrl = "https://assets.bonappetit.com/photos/5d4ddd5c3bde88000879f7ce/16:9/w_1600%2Cc_limit/0919-BA-Sweet-Saucy-Pork-Chop-Playbook.jpg",
                steps = listOf(
                    RecipeStep(0, "First step"),
                    RecipeStep(1, "Next step")
                )
            )
        ),
        HomeRecyclerViewAdapter.Item.RecipeItem(
            Recipe(
                id = 3, heading = "Fourth Recipe", imageUrl = "https://assets.bonappetit.com/photos/5d6834f3b7a6360008a21ae4/16:9/w_2560%2Cc_limit/BA-0919-Ed-Letter.jpg", steps = listOf(
                    RecipeStep(0, "First step"),
                    RecipeStep(1, "Next step")
                )
            )
        )
    )
}