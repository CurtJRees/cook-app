package com.curtjrees.cookapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Recipe(
    val id: Int,
    val imageUrl: String,
    val heading: String,
    val steps: List<RecipeStep>
): Parcelable

@Parcelize
data class RecipeStep(
    val orderNum: Int,
    val text: String, //TODO: Update this
    val imageUrl: String? = null
): Parcelable

//TODO: Move this into a mock repo/service
val mockList = listOf(
    Recipe(
        id = 0, heading = "First Recipe", imageUrl = "https://assets.bonappetit.com/photos/5c9919258e28c80391bd50de/16:9/w_2560,c_limit/chicken-kastsu-2.jpg", steps = listOf(
            RecipeStep(0, "First step"),
            RecipeStep(1, "Next step")
        )
    ),
    Recipe(
        id = 1, heading = "Second Recipe", imageUrl = "https://assets.bonappetit.com/photos/5d56aa938bcd000008067173/16:9/w_1600%2Cc_limit/Basically-Ribs-Macro.jpg", steps = listOf(
            RecipeStep(0, "First step"),
            RecipeStep(1, "Next step")
        )
    )
)